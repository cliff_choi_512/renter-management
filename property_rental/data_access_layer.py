from abc import abstractmethod

from .models import Person, Property, Rental


class Crud:
    @abstractmethod
    def create(self, *args, **kwargs):
        pass

    @abstractmethod
    def update(self, *args, **kwargs):
        pass

    @abstractmethod
    def read(self, *args, **kwargs):
        pass

    @abstractmethod
    def delete(self, *args, **kwargs):
        pass


class PersonCrud(Crud):
    def create(self, first_name: str, last_name: str, email: str, birthday: str, **kwargs):
        middle_name = kwargs.get('middle_name')

        # make sure empty strings or None values are set to none
        if not middle_name:
            middle_name = None

        new_person = Person(
            first_name=first_name,
            middle_name=middle_name,
            last_name=last_name,
            email=email,
            birthday=birthday
        )
        new_person.save()
        return new_person.id

    def read(self, person_id: int):
        return Person.objects.filter(pk=person_id).first()

    def read_multiple(self, person_ids):
        return Person.objects.filter(pk__in=person_ids).all()

    def update(self, person_id: int, **kwargs):
        middle_name = kwargs.get('middle_name')
        # make sure empty strings or None values are set to none
        if not middle_name:
            kwargs.update(middle_name=None)

        Person.objects.filter(pk=person_id).update(**kwargs)
        return person_id

    def delete(self, person_id: int):
        Person.objects.filter(pk=person_id).delete()
        return person_id


class PropertyCrud(Crud):
    def create(self, address: str, name: str, price: float):
        new_property = Property(
            address=address,
            name=name,
            price=price,
        )
        new_property.save()
        return new_property.id

    def update(self, property_id: int, **kwargs):
        Property.objects.filter(pk=property_id).update(**kwargs)
        return property_id

    def read(self, property_id: int):
        return Property.objects.filter(pk=property_id).first()

    def readAll(self):
        return Property.objects.all()

    def delete(self, property_id: int):
        Property.objects.filter(pk=property_id).delete()
        return property_id


class RentalCrud(Crud):
    def create(self, occupants, property, start_date, end_date):
        new_rental = Rental(
            start_time=start_date,
            end_time=end_date,
            property=property
        )

        new_rental.save()

        new_rental.occupants.add(*occupants)
        new_rental.save()
        return new_rental.id

    def update(self, rental_id: int, **kwargs):
        Rental.objects.filter(pk=rental_id).update(**kwargs)
        return rental_id

    def read(self, rental_id: int):
        return Rental.objects.filter(pk=rental_id).first()

    def delete(self, rental_id: int):
        Property.objects.filter(pk=rental_id).delete()
        return rental_id
