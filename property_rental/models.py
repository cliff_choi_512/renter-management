from django.db import models


# Create your models here.
class Property(models.Model):
    address = models.CharField(max_length=128, blank=False)
    name = models.CharField(max_length=128, blank=False)
    price = models.FloatField()

    def __str__(self):
        return self.name

class Person(models.Model):
    first_name = models.CharField(max_length=128, blank=False)
    middle_name = models.CharField(null=True, max_length=128, default=None, blank=False)
    last_name = models.CharField(max_length=128, blank=False)
    email = models.EmailField(null=True, default=None, blank=False)
    birthday = models.DateField()

    def __str__(self):
        return self.last_name + ", " + self.first_name


class Rental(models.Model):
    occupants = models.ManyToManyField(Person)
    property = models.ForeignKey(Property, on_delete=models.CASCADE)
    start_time = models.DateField()
    end_time = models.DateField()

    def __str__(self):
        return str(self.property) + ", " + self.start_time + " - " + self.end_time
