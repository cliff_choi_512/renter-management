# Generated by Django 3.1.7 on 2021-04-02 22:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=128)),
                ('middle_name', models.CharField(max_length=128, null=True)),
                ('last_name', models.CharField(max_length=128)),
                ('email', models.EmailField(max_length=254, null=True)),
                ('birthday', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='Property',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=128)),
                ('name', models.CharField(max_length=128)),
                ('price', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Rental',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_time', models.DateField()),
                ('end_time', models.DateField()),
                ('occupants', models.ManyToManyField(to='property_rental.Person')),
                ('property', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='property_rental.property')),
            ],
        ),
    ]
