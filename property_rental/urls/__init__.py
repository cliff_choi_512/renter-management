from django.urls import path, include

urlpatterns = [
    path('test/', include('property_rental.urls.smoke_responses')),
    path('api/', include('property_rental.urls.restful')),
    path('', include('property_rental.urls.pages')),
]
