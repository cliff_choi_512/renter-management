from django.urls import path
from ..views import smoke_responses

urlpatterns = [
    path('response', smoke_responses.get_request, name='test_responses.get_request'),
    path('page', smoke_responses.page, name='test_responses.page'),
]