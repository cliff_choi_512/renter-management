from django.urls import path
from property_rental.views import property_restful

urlpatterns = [
    path('', property_restful.property, name='property'),
]