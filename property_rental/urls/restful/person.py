from django.urls import path
from property_rental.views import person_restful

urlpatterns = [
    path('', person_restful.person, name='person'),
]