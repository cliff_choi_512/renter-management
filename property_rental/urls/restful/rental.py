from django.urls import path
from property_rental.views import rental_restful, rental_views

urlpatterns = [
    path('', rental_restful.rental, name='rental'),
]