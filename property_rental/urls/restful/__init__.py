from django.urls import path, include

urlpatterns = [
    path('person/', include('property_rental.urls.restful.person')),
    path('property/', include('property_rental.urls.restful.property')),
    path('rental/', include('property_rental.urls.restful.rental')),
]
