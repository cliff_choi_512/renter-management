from django.urls import path
from property_rental.views import rental_views

urlpatterns = [
    path('', rental_views.page, name='page'),
]