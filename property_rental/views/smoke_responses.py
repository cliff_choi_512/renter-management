from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.

def get_request(request):
    return HttpResponse("Hello, everyone. This is a test get response. It is working.")


def page(request):
    return render(request=request, template_name='smoke_page.html')
