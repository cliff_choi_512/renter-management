from django.http import JsonResponse, HttpResponseNotAllowed, HttpResponseBadRequest

from ..data_access_layer import PropertyCrud
import json

property_crud = PropertyCrud()


def property(request):
    if request.method == 'GET':
        return __property_get(request)
    elif request.method == 'POST':
        return __property_post(request)
    elif request.method == 'PUT':
        return __property_put(request)
    elif request.method == 'DELETE':
        return __property_delete(request)
    else:
        return HttpResponseNotAllowed(permitted_methods=['GET', 'POST', 'PUT', 'DELETE'])


def __property_get(request):
    property_id = request.GET['id']
    property_object = property_crud.read(property_id=property_id)

    if not property_object:
        return JsonResponse(
            data={'message': "Property not found"},
            status=400
        )

    return JsonResponse({
        'id': property_object.id,
        'address': property_object.address,
        'name': property_object.name,
        'price': property_object.price,
    })


def __property_post(request):
    body = json.loads(request.body)
    property_id = property_crud.create(
        address=body['address'],
        name=body['name'],
        price=body['price'],
    )
    return JsonResponse({'id': property_id, 'message': 'Successfully created property.'})


def __property_put(request):
    data = json.loads(request.body)
    property_id = data['id']
    property_id = property_crud.update(property_id=property_id, **data)
    return JsonResponse({'id': property_id, 'message': 'Successfully updated property.'})


def __property_delete(request):
    data = json.loads(request.body)
    property_id = data['id']
    property_deleted_id = property_crud.delete(property_id=property_id)

    return JsonResponse({'id': property_deleted_id, 'message': 'Successfully deleted property.'})
