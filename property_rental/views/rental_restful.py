from django.http import JsonResponse, HttpResponseNotAllowed, HttpResponseBadRequest

from ..data_access_layer import RentalCrud, PersonCrud, PropertyCrud

import json

rental_crud = RentalCrud()
person_crud = PersonCrud()
property_crud = PropertyCrud()


def rental(request):
    if request.method == 'GET':
        return __rental_get(request)
    elif request.method == 'POST':
        return __rental_post(request)
    elif request.method == 'PUT':
        return __rental_put(request)
    elif request.method == 'DELETE':
        return __rental_delete(request)
    else:
        return HttpResponseNotAllowed(permitted_methods=['GET', 'POST', 'PUT', 'DELETE'])


def __rental_get(request):
    rental_id = request.GET['id']
    rental_object = rental_crud.read(rental_id=rental_id)

    if not rental_object:
        return JsonResponse(
            data={'message': "rental not found"},
            status=400
        )

    return JsonResponse({
        'id': rental_object.id,
        # 'occupants': rental_object.occupants,
        'property_id': rental_object.property.id,
        'property_name': rental_object.property.name,
        'start_time': rental_object.start_time,
        'end_time': rental_object.end_time,
    })


def __rental_post(request):
    body = json.loads(request.body)
    persons = body['persons']
    persons = person_crud.read_multiple(persons)

    property_id = body['property']
    property_object = property_crud.read(property_id)

    start_date = body['start_date']
    end_date = body['end_date']

    rental_id = rental_crud.create(
        start_date=start_date,
        end_date=end_date,
        property=property_object,
        occupants=persons,
    )
    return JsonResponse({'id': rental_id, 'message': 'Successfully created rental.'})


def __rental_put(request):
    data = json.loads(request.body)
    rental_id = data['id']
    rental_id = rental_crud.update(rental_id=rental_id, **data)
    return JsonResponse({'id': rental_id, 'message': 'Successfully updated rental.'})


def __rental_delete(request):
    data = json.loads(request.body)
    rental_id = data['id']
    rental_deleted_id = rental_crud.delete(rental_id=rental_id)

    return JsonResponse({'id': rental_deleted_id, 'message': 'Successfully deleted rental.'})

