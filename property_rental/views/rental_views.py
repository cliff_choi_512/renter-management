from django.http import HttpResponse
from django.template import loader

from ..models import *
from ..data_access_layer import PropertyCrud

property_crud = PropertyCrud()

def page(request):
    property_list = property_crud.readAll()
    context = {
        'properties': property_list
    }
    template = loader.get_template("rental.html")

    # Render the response and send it back!
    return HttpResponse(template.render(context, request))
