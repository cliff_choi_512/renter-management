from django.http import JsonResponse, HttpResponseNotAllowed

from ..data_access_layer import PersonCrud
import json

user_crud = PersonCrud()


def person(request):
    if request.method == 'GET':
        return __person_get(request)
    elif request.method == 'POST':
        return __person_post(request)
    elif request.method == 'PUT':
        return __person_put(request)
    elif request.method == 'DELETE':
        return __person_delete(request)
    else:
        return HttpResponseNotAllowed(permitted_methods=['GET', 'POST', 'PUT', 'DELETE'])


def __person_get(request):
    person_id = request.GET['id']
    person_object = user_crud.read(person_id=person_id)

    if not person_object:
        return JsonResponse(
            data={'message': "Person not found"},
            status=400
        )

    return JsonResponse({
        'id': person_object.id,
        'first_name': person_object.first_name,
        'middle_name': person_object.middle_name,
        'last_name': person_object.last_name,
        'email': person_object.email,
        'birthday': person_object.birthday.isoformat(),
    })


def __person_post(request):
    data = json.loads(request.body)
    person_id = user_crud.create(
        first_name=data['first_name'],
        middle_name=data['middle_name'],
        last_name=data['last_name'],
        email=data['email'],
        birthday=data['birthday'],
    )
    return JsonResponse({'id': person_id, 'message': 'Successfully created person.'})


def __person_put(request):
    data = json.loads(request.body)
    person_id = data['id']
    person_id = user_crud.update(person_id=person_id, **data)
    return JsonResponse({'id': person_id, 'message': 'Successfully updated person.'})


def __person_delete(request):
    data = json.loads(request.body)
    person_id = data['id']
    person_deleted_id = user_crud.delete(person_id=person_id)

    return JsonResponse({'id': person_deleted_id, 'message': 'Successfully deleted person.'})
