from django.test import SimpleTestCase


class SmokeResponsesTests(SimpleTestCase):

    def test_get_response(self):
        response = self.client.get('/test/response')
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'Hello, everyone. This is a test get response. It is working.')

    def test_page(self):
        response = self.client.get('/test/page')
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, '<h1>This is a test page. It is displaying correctly.</h1>')
        self.assertTemplateUsed(response, 'smoke_page.html')
