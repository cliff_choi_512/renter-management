from django.test import TestCase
from ..data_access_layer import PersonCrud


class PersonCrudTests(TestCase):
    def setUp(self):
        self.property_crud = PersonCrud()

    def test_all_param(self):
        first_name = "test_all_param"
        middle_name = "SomeMiddleName"
        last_name = "UserCrudTests"
        email = "test_all_param@user_crud_tests.com"
        birthday = "1990-01-01"

        person_id = self.property_crud.create(
            first_name=first_name,
            middle_name=middle_name,
            last_name=last_name,
            email=email,
            birthday=birthday,
        )

        resulting_person = self.property_crud.read(person_id=person_id)
        self.assertEquals(first_name, resulting_person.first_name)
        self.assertEquals(middle_name, resulting_person.middle_name)
        self.assertEquals(last_name, resulting_person.last_name)
        self.assertEquals(email, resulting_person.email)
        self.assertEquals(birthday, resulting_person.birthday.isoformat())

        first_name = "test_ALL_param"
        middle_name = "MiddleGround"
        last_name = "uSerCruDTests"
        email = "test_all_param@user_crud_tests.de"
        birthday = "1996-02-29"

        self.property_crud.update(
            person_id=person_id,
            first_name=first_name,
            middle_name=middle_name,
            last_name=last_name,
            email=email,
            birthday=birthday,
        )

        resulting_person = self.property_crud.read(person_id=person_id)
        self.assertEquals(first_name, resulting_person.first_name)
        self.assertEquals(middle_name, resulting_person.middle_name)
        self.assertEquals(last_name, resulting_person.last_name)
        self.assertEquals(email, resulting_person.email)
        self.assertEquals(birthday, resulting_person.birthday.isoformat())

        self.property_crud.delete(person_id=person_id)
        resulting_person = self.property_crud.read(person_id=person_id)
        self.assertIsNone(resulting_person, "Person Object may NOT been deleted. You may need to manually delete "
                                            "field at index %d" % person_id)

    def test_min_param(self):
        first_name = "test_min_param"
        last_name = "UserCrudTests"
        email = "test_min_param@user_crud_tests.com"
        birthday = "1990-01-01"

        person_id = self.property_crud.create(
            first_name=first_name,
            last_name=last_name,
            email=email,
            birthday=birthday,
        )

        resulting_person = self.property_crud.read(person_id=person_id)
        self.assertEquals(first_name, resulting_person.first_name)
        self.assertIsNone(resulting_person.middle_name)
        self.assertEquals(last_name, resulting_person.last_name)
        self.assertEquals(email, resulting_person.email)
        self.assertEquals(birthday, resulting_person.birthday.isoformat())

        first_name = "something_has_changed"

        self.property_crud.update(
            person_id=person_id,
            first_name=first_name
        )

        resulting_person = self.property_crud.read(person_id=person_id)
        self.assertEquals(first_name, resulting_person.first_name)
        self.assertIsNone(resulting_person.middle_name)
        self.assertEquals(last_name, resulting_person.last_name)
        self.assertEquals(email, resulting_person.email)
        self.assertEquals(birthday, resulting_person.birthday.isoformat())

        self.property_crud.delete(person_id=person_id)
        resulting_person = self.property_crud.read(person_id=person_id)
        self.assertIsNone(resulting_person,
                          "Person Object may NOT been deleted. You may need to manually delete field at index %d" % person_id)

    def test_blank_middle_name(self):
        first_name = "test_blank_middle_name"
        middle_name = ""
        last_name = "UserCrudTests"
        email = "test_blank_middle_namem@user_crud_tests.com"
        birthday = "1990-01-01"

        person_id = self.property_crud.create(
            first_name=first_name,
            middle_name=middle_name,
            last_name=last_name,
            email=email,
            birthday=birthday,
        )

        resulting_person = self.property_crud.read(person_id=person_id)
        self.assertIsNone(resulting_person.middle_name)

        middle_name = ""

        self.property_crud.update(
            person_id=person_id,
            middle_name=middle_name
        )

        resulting_person = self.property_crud.read(person_id=person_id)
        self.assertIsNone(resulting_person.middle_name)

        self.property_crud.delete(person_id=person_id)
        resulting_person = self.property_crud.read(person_id=person_id)
        self.assertIsNone(resulting_person, "Person Object may NOT been deleted. You may need to manually delete "
                                            "field at index %d" % person_id)
        
class PersonRestfulAPITests(TestCase):
    def test_can_insert_read_update_delete(self):
        first_name = "1"
        middle_name = "LongMiddleName"
        last_name = "PersonRestfulAPITests"
        email = "test_can_insert_read_update_delete@person_restful_api_tests.com"
        birthday = "1900-01-01"

        # create user
        request_body = {
            'first_name': first_name,
            'middle_name': middle_name,
            'last_name': last_name,
            'email': email,
            'birthday': birthday
        }
        response = self.client.post(
            path='/api/person/',
            data=request_body,
            content_type='application/json'
        )
        self.assertEquals(200, response.status_code)
        person_id = response.json()['id']

        response = self.client.get(
            path='/api/person/',
            data={'id': person_id},
        )
        self.assertEquals(200, response.status_code)
        response_body = response.json()
        self.assertEquals(person_id, response_body['id'])
        self.assertEquals(first_name, response_body['first_name'])
        self.assertEquals(middle_name, response_body['middle_name'])
        self.assertEquals(last_name, response_body['last_name'])
        self.assertEquals(email, response_body['email'])
        self.assertEquals(birthday, response_body['birthday'])

        # update all parameters

        first_name = "?"
        middle_name = "w"
        last_name = "test_can_insert_read_update_delete-person_restful_api_tests"
        email = "a@a.al"
        birthday = "2020-01-01"

        # create user
        request_body = {
            'id': person_id,
            'first_name': first_name,
            'middle_name': middle_name,
            'last_name': last_name,
            'email': email,
            'birthday': birthday
        }

        response = self.client.put(
            path='/api/person/',
            data=request_body,
            content_type='application/json'
        )
        self.assertEquals(200, response.status_code)
        response_body = response.json()
        self.assertEquals(person_id, response_body['id'])
        self.assertIsNotNone(response_body['message'])

        response = self.client.get(
            path='/api/person/',
            data={'id': person_id},
        )
        self.assertEquals(200, response.status_code)
        response_body = response.json()
        self.assertEquals(person_id, response_body['id'])
        self.assertEquals(first_name, response_body['first_name'])
        self.assertEquals(middle_name, response_body['middle_name'])
        self.assertEquals(last_name, response_body['last_name'])
        self.assertEquals(email, response_body['email'])
        self.assertEquals(birthday, response_body['birthday'])

        # delete
        response = self.client.delete(
            path='/api/person/',
            data={'id': person_id},
            content_type='application/json',
        )
        self.assertEquals(200, response.status_code)
        response_body = response.json()
        self.assertEquals(person_id, response_body['id'])
        self.assertIsNotNone(response_body['message'])

        response = self.client.get(
            path='/api/person/',
            data={'id': person_id},
        )
        self.assertEquals(400, response.status_code)
        response_body = response.json()
        self.assertIsNotNone(response_body['message'])

    def test_return_405_when_method_not_supported(self):
        response = self.client.patch(
            path='/api/person/',
            data={},
            content_type='application/json'
        )
        self.assertEquals(405, response.status_code)