from django.test import TestCase, SimpleTestCase
from ..data_access_layer import PropertyCrud


class PropertyCrudTests(TestCase):
    def setUp(self):
        self.property_crud = PropertyCrud()

    def test_can_insert_read_update_delete(self):
        address = "647 Everton Rd. Los Angeles, California"
        name = "Small Condo"
        price = 1500

        property_id = self.property_crud.create(
            address=address,
            name=name,
            price=price,
        )

        resulting_property = self.property_crud.read(property_id=property_id)
        self.assertEquals(address, resulting_property.address)
        self.assertEquals(name, resulting_property.name)
        self.assertEquals(price, resulting_property.price)

        address = "Conradstrasse 341, Stuttgart, Baden-Wuttemburg, DE"
        name = "Beautiful Condo in the Hills of Stuttgart"
        price = 819.25

        self.property_crud.update(
            property_id=property_id,
            address=address,
            name=name,
            price=price,
        )

        resulting_property = self.property_crud.read(property_id=property_id)
        self.assertEquals(address, resulting_property.address)
        self.assertEquals(name, resulting_property.name)
        self.assertEquals(price, resulting_property.price)

        name = "Turned out the property is in the Stuttgart Zentrum!"

        self.property_crud.update(
            property_id=property_id,
            name=name,
        )

        resulting_property = self.property_crud.read(property_id=property_id)
        self.assertEquals(address, resulting_property.address)
        self.assertEquals(name, resulting_property.name)
        self.assertEquals(price, resulting_property.price)

        self.property_crud.delete(property_id=property_id)
        resulting_property = self.property_crud.read(property_id=property_id)
        self.assertIsNone(resulting_property, "Person Object may NOT been deleted. You may need to manually delete "
                                              "field at index %d" % property_id)


class PropertyRestfulAPITests(TestCase):
    def test_can_insert_read_update_delete(self):
        address = "Rue Gardiner 2932, Marseilles"
        name = "PropertyRestfulApiTests-test_can_insert_read_update_delete-part1"
        price = 429.99

        # create user
        request_body = {
            'address': address,
            'name': name,
            'price': price
        }
        response = self.client.post(
            path='/api/property/',
            data=request_body,
            content_type='application/json'
        )
        self.assertEquals(200, response.status_code)
        property_id = response.json()['id']

        response = self.client.get(
            path='/api/property/',
            data={'id': property_id},
        )
        self.assertEquals(200, response.status_code)
        response_body = response.json()
        self.assertEquals(property_id, response_body['id'])
        self.assertEquals(address, response_body['address'])
        self.assertEquals(name, response_body['name'])
        self.assertEquals(price, response_body['price'])

        # update all parameters
        address = "Marktstraat 3 Rotterdam, Zuid-Holland"
        name = "PropertyRestfulApiTests-test_can_insert_read_update_delete-part2"
        price = 849.5

        request_body = {
            'id': property_id,
            'address': address,
            'name': name,
            'price': price,
        }

        response = self.client.put(
            path='/api/property/',
            data=request_body,
            content_type='application/json'
        )
        self.assertEquals(200, response.status_code)
        response_body = response.json()
        self.assertEquals(property_id, response_body['id'])
        self.assertIsNotNone(response_body['message'])

        response = self.client.get(
            path='/api/property/',
            data={'id': property_id},
        )
        self.assertEquals(200, response.status_code)
        response_body = response.json()
        self.assertEquals(address, response_body['address'])
        self.assertEquals(name, response_body['name'])
        self.assertEquals(price, response_body['price'])

        # delete
        response = self.client.delete(
            path='/api/property/',
            data={'id': property_id},
            content_type='application/json',
        )
        self.assertEquals(200, response.status_code)
        response_body = response.json()
        self.assertEquals(property_id, response_body['id'])
        self.assertIsNotNone(response_body['message'])

        response = self.client.get(
            path='/api/property/',
            data={'id': property_id},
        )
        self.assertEquals(400, response.status_code)
        response_body = response.json()
        self.assertIsNotNone(response_body['message'])

    def test_return_405_when_method_not_supported(self):
        response = self.client.patch(
            path='/api/property/',
            data={},
            content_type='application/json'
        )
        self.assertEquals(405, response.status_code)

